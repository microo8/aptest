module github.com/microo8/aptest

go 1.13

require (
	github.com/captncraig/cors v0.0.0-20190703115713-e80254a89df1 // indirect
	github.com/go-fed/activity v0.4.0
	github.com/gorilla/mux v1.7.3
	github.com/pkg/errors v0.8.1 // indirect
	github.com/writeas/go-nodeinfo v1.0.0
	github.com/writeas/go-webfinger v1.1.0
	github.com/writeas/httpsig v1.0.0
)

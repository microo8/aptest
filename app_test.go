package aptest

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/microo8/aptest/models"
)

func init() {
	log.SetFlags(log.Ltime | log.Lshortfile)
}

func makeRequest(handler http.Handler, method, url, data string) (*http.Response, error) {
	buf := bytes.NewBuffer([]byte(data))
	req, err := http.NewRequest(method, url, buf)
	if err != nil {
		return nil, fmt.Errorf("cannot create request: %w", err)
	}
	req.Header.Add("Content-Type", "application/activity+json")
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)
	return rr.Result(), nil
}

func TestCreate(t *testing.T) {
	storage, _ := models.NewMockStorage()
	app := &app{
		cfg:     &config{name: "aptest"},
		storage: storage,
	}
	initRoutes(app)

	t.Run("Link", func(t *testing.T) {
		resp, err := makeRequest(app.router, "POST", "/api/mocker/outbox", `{
			"@context": "https://www.w3.org/ns/activitystreams",
			"type": "Create",
			"actor": "https://localhost/api/mocker",
			"object": {
				"type": "Link",
				"attributedTo": "https://localhost/api/mocker",
				"href": "https://google.com",
				"to": ["https://localhost/api/example_comunity"]
			},
			"to": ["https://localhost/api/example_comunity"]
		}`)
		if err != nil {
			t.Fatalf("making request create link activity: %s", err)
		}
		if resp.StatusCode != http.StatusAccepted {
			body, _ := ioutil.ReadAll(resp.Body)
			t.Errorf("create link activity returned wrong status code: %s (%s)", resp.Status, string(body))
		}
	})
}

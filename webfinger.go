package aptest

import (
	"fmt"
	"strings"

	"github.com/writeas/go-webfinger"
)

func (app *app) FindUser(username string, host, requestHost string, r []webfinger.Rel) (*webfinger.Resource, error) {
	realHost := app.cfg.host[strings.LastIndexByte(app.cfg.host, '/')+1:]
	if host != realHost {
		return nil, fmt.Errorf("Host doesn't match")
	}

	u, err := app.storage.GetLocalUser(username)
	if err != nil {
		return nil, err
	}

	profileURL := app.cfg.host + "/" + username
	res := webfinger.Resource{
		Subject: "acct:" + username + "@" + host,
		Aliases: []string{
			profileURL,
			localUserAccountRoot(u, app.cfg),
		},
		Links: []webfinger.Link{
			{
				HRef: profileURL,
				Type: "text/html",
				Rel:  "https://webfinger.net/rel/profile-page",
			},
			{
				HRef: localUserAccountRoot(u, app.cfg),
				Type: "application/activity+json",
				Rel:  "self",
			},
		},
	}
	return &res, nil

}

func (app *app) DummyUser(username string, hostname string, r []webfinger.Rel) (*webfinger.Resource, error) {
	return nil, fmt.Errorf("User not found.")
}

func (app *app) IsNotFoundError(err error) bool {
	return err != nil && err.Error() == "User not found."
}

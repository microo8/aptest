package main

import (
	"log"

	"github.com/microo8/aptest"
	"github.com/microo8/aptest/models"
)

func main() {
	/*
		dbConnInfo := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
			"root",
			"meh",
			"localhost",
			"5432",
			"aptest",
		)
	*/
	log.SetFlags(log.Ltime | log.Lshortfile)

	storage, err := models.NewMockStorage()
	if err != nil {
		panic(err)
	}
	aptest.Serve(storage)
}

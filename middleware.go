package aptest

import (
	"context"
	"net/http"

	"github.com/microo8/aptest/models"
)

func (app *app) middleware(h handlerFunc) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ctx = context.WithValue(ctx, "cfg", app.cfg)
		ctx = context.WithValue(ctx, "storage", app.storage)
		handleError(w, r, h(w, r.WithContext(ctx)))
	})
}

func cfg(ctx context.Context) *config {
	return ctx.Value("cfg").(*config)
}

func storage(ctx context.Context) models.Storage {
	return ctx.Value("storage").(models.Storage)
}

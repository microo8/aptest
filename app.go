package aptest

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/writeas/go-nodeinfo"
	"github.com/writeas/go-webfinger"

	"github.com/microo8/aptest/models"
)

type app struct {
	router  *mux.Router
	storage models.Storage
	cfg     *config
	//keys   *keychain
}
type config struct {
	name    string //app name
	version string
	host    string
	port    int
}

func Serve(storage models.Storage) {
	app := &app{
		cfg:     &config{name: "aptest"},
		storage: storage,
	}

	flag.IntVar(&app.cfg.port, "p", 8080, "server port")
	flag.StringVar(&app.cfg.host, "h", "https://localhost", "site's base URL")
	flag.Parse()

	initRoutes(app)

	http.Handle("/", app.router)
	log.Printf("Serving on localhost:%d", app.cfg.port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", app.cfg.port), nil))
}

func initRoutes(app *app) {
	app.router = mux.NewRouter()

	// Federation endpoints
	wf := webfinger.Default(app)
	wf.NoTLSHandler = nil
	// host-meta
	app.router.Handle("/.well-known/host-meta", app.middleware(handleViewHostMeta))
	// webfinger
	app.router.Handle(webfinger.WebFingerPath, http.HandlerFunc(wf.Webfinger))
	// nodeinfo
	niCfg := app.nodeInfoConfig()
	ni := nodeinfo.NewService(*niCfg, app.nodeInfoResolver())
	app.router.Handle(nodeinfo.NodeInfoPath, http.HandlerFunc(ni.NodeInfoDiscover))
	//app.router.Handle(niCfg.InfoURL, http.HandlerFunc(ni.NodeInfo))

	api := app.router.PathPrefix("/api/{alias}").Subrouter()
	api.Handle("/", app.middleware(handleFetchUser)).Methods("GET")
	api.Handle("/inbox", app.middleware(handleInbox)).Methods("POST")
	api.Handle("/outbox", app.middleware(handleFetchOutbox)).Methods("GET")
	api.Handle("/outbox", app.middleware(handlePostOutbox)).Methods("POST")
	//collectionsAPI.Handle("/following", app.middleware(handleFetchFollowing)).Methods("GET")
	//collectionsAPI.Handle("/followers", app.middleware(handleFetchFollowers)).Methods("GET")

	//api.Handle("/follow", app.middleware(handleFollowUser))
}

func handleViewHostMeta(w http.ResponseWriter, r *http.Request) error {
	cfg := cfg(r.Context())
	w.Header().Set("Server", cfg.name)
	w.Header().Set("Content-Type", "application/xrd+xml; charset=utf-8")

	meta := `<?xml version="1.0" encoding="UTF-8"?>
				<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
				  <Link rel="lrdd" type="application/xrd+xml" template="` + cfg.host + `/.well-known/webfinger?resource={uri}"/>
				  </XRD>`
	fmt.Fprintf(w, meta)
	return nil
}

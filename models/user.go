package models

import "github.com/microo8/aptest/as"

type User struct {
	as.Person
	ID string `json:"-"`
}

func (u *User) AsPerson() *as.Person {
	p := u.Person
	p.Context = []interface{}{
		as.Namespace,
	}
	return &p
}

// LocalUser is a local user
type LocalUser struct {
	ID                string `json:"-"`
	PreferredUsername string `json:"preferredUsername"`
	HashedPass        []byte `json:"-"`
	Name              string `json:"name"`
	Summary           string `json:"summary"`
	PrivKey           []byte
	PubKey            []byte
}

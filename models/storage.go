package models

import (
	"github.com/microo8/aptest/as"
)

type Storage interface {
	GetLocalUser(username string) (*LocalUser, error)
	GetUsersCount() (uint64, error)
	GetActor(id string) (*User, error)
	AddUser(u *as.Person) (string, error)
	AddFollow(uid, remoteuid string) error
	RemoveFollow(uid, remoteuid string) error
	CreatePost(comunity, author, title, url string) error
	CreateComunity(name, summary, moderator string) error
}

type APError int

const (
	ErrNotFound APError = iota
)

func (err APError) Error() string {
	switch err {
	case ErrNotFound:
		return "NotFound"
	}
	return "error"
}

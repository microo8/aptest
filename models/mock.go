package models

import (
	"github.com/microo8/aptest/as"
)

type MockStorage struct{}

func NewMockStorage() (Storage, error) {
	return &MockStorage{}, nil
}

func (ms *MockStorage) GetLocalUser(username string) (*LocalUser, error) {
	if username != "mocker" {
		return nil, ErrNotFound
	}
	return &LocalUser{
		ID:                "123",
		HashedPass:        []byte("123"),
		Name:              "mocker user",
		PreferredUsername: "mocker",
		PrivKey: []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAtkcItHygZ4UAlVxvvVT6XR2KZ7WkPugEFWxXgrECe+5bFrm3
9mFu1Z3QjfZQPhFS5PitHiIkYbXzGC/XZrgVVx2lEwWf3IREaONAHz9aM4s0IvsE
gIK2Xv5NMB6msqCyOIQAkoh/gqD2pPqpGEw0RFKvn3lgqDnwTHPr6q6F19H6GBKd
zr5wKoNFmhtwLIYyoPmCwUWknkGLWjgSiKQhTqAZ0N8fT56OUweXffzb2I0iymVc
up4N2A6i1mwtG1t1+QJBZ2jLpMBCY7uwF0LKEK99eSeB44+XQaOGpGnXM9IYmm4n
hIWxFUaUeJw07HFD25YVL/B1g2nJ7XGziCTZ/wIDAQABAoIBAQCCa5kC6xkqeLPO
KvGYmrD45yYcxo7ao9AoM/j5fgW0/vsV65SBKX3QQrbynIcVofeZ9YSIxw0ZDML/
T1vkF1rNllyCm8eiVNFN6yv6L64uqy+Gx+yUtvIeTf6Uq5nCvdBHN8vhDOGPafSQ
XvmWdRAAwtj+Vi6er7YMPfVTWcRSgqjnoOUb1WtGvSrQ3Cqrya99LmV4BkBJhEnS
YsyRWkMdIUn4+aNVZSqBnwAd0oCk1Gjgq+if+9Mcp8MSsmBbe0ZgOCsoEihJ5mGG
ERj7Oa1vV6derTmz5sBk0w4MTbB00P4V8AlGwcVFGcBFv2W550ehbZxo3Zl3ImM8
A5DPXM1BAoGBAOz63/a1x9yYXmvKQo+mX7+tC3OriPdTpIwGCRV9iq9alLxTb/OZ
BKHGNQ4DPGIkVyK4oGy9YN9YxaEI1g/8U//UFfdEwkYzBnhAdkUEM7DCnEjS9e09
xlXZ6vOjJTuAoWUw5J3c4SNOz/una/KqM2JO5KFxevVmB9VmiPzqidvnAoGBAMTo
NeFxb0E+MtF7FHO683wFhUS/gKp/XovFwa0S7G+IXKYhdQsX7Yo+ouDYtDYOJ0tb
4ZTAymMlA/VoZ94F1K3rdHMDgM5b6tyFvlvq4q2NuaK4CBZmPJ71fhP1WKEDvZR5
d4xILMDstLeF9d5S2sCuL1tLXWarkRxuw26wZg4pAoGAJkZqz/USP73YWj6Du6Ha
iyHO3LLS32eCBx+ebfsl4CaYKUasqUKIrfxOa2H4ab2G2VPKjWWU9N3opZbAxDoz
8vj1R0eyvRXWsKtwrrr8AkucZGK2V8gHdaxJt2odRFwFNUvNUPXw4vwCuZmT5NC6
leE6Kh0jB+mf0EbMT1UuSPcCgYAKbDQ167g03uL3vakWeyXaQbyNKAnafIV3po8a
5y6GtNQaoxi77pdPQwOCHVZkqhkgXwuwwQe1eccc3VrqIKriTd1LHTtbMPhcleO+
n89B2oeh+lXLQbwhueLjkZAQbc9CJa/TSBq+V4k1UazzCLW1j0LIdVPLVHeI5sAH
guTUCQKBgFRGpv1l4t2sK3o7DgXh47tlKCzD0vc9sTM4l7RdE+8NszpdquBPlACZ
VFZtnzu1ikCXNbnann0/VA/OlHywxYSKU689R/mZk0HLcZrBt+yRpn4cOZ2Viz8a
NGzA8WLskBrz+BBbIr/bE7PqBj/MnSv5pE6VfWRe96dvBwVqqtE+
-----END RSA PRIVATE KEY-----`),
		PubKey: []byte(`-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtkcItHygZ4UAlVxvvVT6
XR2KZ7WkPugEFWxXgrECe+5bFrm39mFu1Z3QjfZQPhFS5PitHiIkYbXzGC/XZrgV
Vx2lEwWf3IREaONAHz9aM4s0IvsEgIK2Xv5NMB6msqCyOIQAkoh/gqD2pPqpGEw0
RFKvn3lgqDnwTHPr6q6F19H6GBKdzr5wKoNFmhtwLIYyoPmCwUWknkGLWjgSiKQh
TqAZ0N8fT56OUweXffzb2I0iymVcup4N2A6i1mwtG1t1+QJBZ2jLpMBCY7uwF0LK
EK99eSeB44+XQaOGpGnXM9IYmm4nhIWxFUaUeJw07HFD25YVL/B1g2nJ7XGziCTZ
/wIDAQAB
-----END PUBLIC KEY-----`),
		Summary: "mocker user for mocking",
	}, nil
}

func (ms *MockStorage) GetUsersCount() (uint64, error) {
	return 1, nil
}

func (ms *MockStorage) GetActor(id string) (*User, error) {
	return &User{}, nil
}

func (ms *MockStorage) AddUser(u *as.Person) (string, error) {
	return "0", nil
}

func (ms *MockStorage) AddFollow(uid, remoteuid string) error {
	return nil
}

func (ms *MockStorage) RemoveFollow(uid, remoteuid string) error {
	return nil
}

func (ms *MockStorage) CreatePost(comunity, author, title, url string) error {
	return nil
}

func (ms *MockStorage) CreateComunity(name, summary, moderator string) error {
	return nil
}

package aptest

import "github.com/writeas/go-nodeinfo"

func (app *app) nodeInfoConfig() *nodeinfo.Config {
	return &nodeinfo.Config{
		BaseURL: app.cfg.host,
		InfoURL: "/api/nodeinfo",

		Metadata: nodeinfo.Metadata{
			NodeName:        app.cfg.name,
			NodeDescription: "ActivityPub-enabled test app.",
			Private:         false,
			Software: nodeinfo.SoftwareMeta{
				HomePage: "https://localhost",
			},
		},
		Protocols: []nodeinfo.NodeProtocol{
			nodeinfo.ProtocolActivityPub,
		},
		Services: nodeinfo.Services{
			Inbound:  []nodeinfo.NodeService{},
			Outbound: []nodeinfo.NodeService{},
		},
		Software: nodeinfo.SoftwareInfo{
			Name:    app.cfg.name,
			Version: app.cfg.version,
		},
	}
}

func (app *app) nodeInfoResolver() nodeinfo.Resolver {
	return nodeInfoResolver{app}
}

type nodeInfoResolver struct{ app *app }

func (r nodeInfoResolver) IsOpenRegistration() (bool, error) {
	return false, nil
}

func (r nodeInfoResolver) Usage() (nodeinfo.Usage, error) {
	users, err := r.app.storage.GetUsersCount()
	return nodeinfo.Usage{
		Users: nodeinfo.UsageUsers{
			Total: int(users),
		},
		LocalPosts: 0,
	}, err
}

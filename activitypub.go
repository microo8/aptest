package aptest

import (
	"bytes"
	"crypto"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"time"

	"github.com/go-fed/activity/streams"
	"github.com/go-fed/activity/vocab"
	"github.com/gorilla/mux"
	"github.com/microo8/aptest/as"
	"github.com/microo8/aptest/models"
	"github.com/writeas/httpsig"
)

type handlerFunc func(w http.ResponseWriter, r *http.Request) error

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	if err == nil {
		return
	}

	/*
		log.Printf("Error: %v", err)
		if err.Status >= 300 && err.Status < 400 {
			impart.WriteRedirect(w, err)
			return
		} else if err.Status == http.StatusUnauthorized {
			if isAPI {
				impart.WriteError(w, err)
			} else {
				impart.WriteRedirect(w, impart.HTTPError{http.StatusFound, "/login?to=" + r.URL.Path})
			}
			return
		}
	*/
	if apErr, ok := err.(models.APError); ok {
		if apErr == models.ErrNotFound {
			http.Error(w, err.Error(), http.StatusNotFound)
		}
	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	return
}

func localUserAsPerson(u *models.LocalUser, cfg *config) *as.Person {
	accountRoot := localUserAccountRoot(u, cfg)
	p := as.NewPerson(accountRoot)
	p.Endpoints.SharedInbox = cfg.host + "/api/inbox"
	p.PreferredUsername = u.PreferredUsername
	p.URL = cfg.host + "/api/" + u.PreferredUsername
	p.Name = u.Name
	p.Summary = u.Summary

	// Add key
	p.Context = append(p.Context, "https://w3id.org/security/v1")
	p.PublicKey = as.PublicKey{
		ID:           p.ID + "#main-key",
		Owner:        p.ID,
		PublicKeyPEM: string(u.PubKey),
	}
	p.SetPrivKey(u.PrivKey)
	return p
}

func localUserAccountRoot(u *models.LocalUser, cfg *config) string {
	return cfg.host + "/api/" + u.PreferredUsername
}

func isLocalUser(cfg *config, userIRI string) (bool, error) {
	u, err := url.Parse(userIRI)
	if err != nil {
		return false, fmt.Errorf("parsing userIRI: %w", err)
	}
	return u.Hostname() == cfg.host, nil
}

func handleFetchUser(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()
	cfg := cfg(ctx)
	storage := storage(ctx)
	w.Header().Set("Server", cfg.name)

	vars := mux.Vars(r)
	alias := vars["alias"]
	u, err := storage.GetLocalUser(alias)
	if err != nil {
		return err
	}
	p := localUserAsPerson(u, cfg)
	return json.NewEncoder(w).Encode(p)
}

func handleFetchOutbox(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()
	cfg := cfg(ctx)
	storage := storage(ctx)
	w.Header().Set("Server", cfg.name)

	vars := mux.Vars(r)
	alias := vars["alias"]

	u, err := storage.GetLocalUser(alias)
	if err != nil {
		return err
	}

	accountRoot := localUserAccountRoot(u, cfg)
	posts := 0 // app.getUserPostsCount(u.PreferredUsername)

	page := r.FormValue("page")
	p, err := strconv.Atoi(page)
	if err != nil || p < 1 {
		// Return outbox
		oc := as.NewOrderedCollection(accountRoot, "outbox", posts)
		return json.NewEncoder(w).Encode(oc)
	}

	// Return outbox page
	ocp := as.NewOrderedCollectionPage(accountRoot, "outbox", posts, p)
	ocp.OrderedItems = []interface{}{}

	/*
		posts, err := app.getUserPosts(u.PreferredUsername)
				for _, p := range *posts {
								o := p.ActivityObject()
											a := as.NewCreateActivity(o)
														ocp.OrderedItems = append(ocp.OrderedItems, *a)
																}
	*/

	return json.NewEncoder(w).Encode(ocp)
}

func handlePostOutbox(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()
	cfg := cfg(ctx)
	storage := storage(ctx)
	//TODO authorize
	w.Header().Set("Server", cfg.name)
	var m map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&m); err != nil {
		return err
	}
	res := &streams.Resolver{
		CreateCallback: func(create *streams.Create) error {
			v := create.Raw()
			if v.ObjectLen() != 1 {
				return fmt.Errorf("create activity must have one object")
			}
			obj := v.GetObject(0)
			switch obj.(type) {
			case vocab.LinkType:
				url := obj.GetUrlAnyURI(0).String()
				title := obj.GetNameString(0)
				author := obj.GetAttributedToIRI(0).String()
				//TODO compare author to authorized user
				comunity := obj.GetToIRI(0).String()
				return storage.CreatePost(comunity, author, title, url)
			case vocab.GroupType:
				name := obj.GetNameString(0)
				summary := obj.GetSummaryString(0)
				moderator := obj.GetAttributedToIRI(0).String()
				//TODO compare moderator to authorized user
				return storage.CreateComunity(name, summary, moderator)
			}
			return nil
		},
		DeleteCallback: func(del *streams.Delete) error {
			return fmt.Errorf("not implemented")
		},
		FollowCallback: func(follow *streams.Follow) error {
			return fmt.Errorf("not implemented")
		},
		UndoCallback: func(undo *streams.Undo) error {
			return fmt.Errorf("not implemented")
		},
	}
	if err := res.Deserialize(m); err != nil {
		// 3) Any errors from #2 can be handled, or the payload is an unknown type.
		log.Printf("ERROR: Unable to resolve: %v", err)
		log.Printf("ERROR: Map: %s", m)
		return err
	}
	w.WriteHeader(http.StatusAccepted)
	return nil
}

func handleInbox(w http.ResponseWriter, r *http.Request) error {
	ctx := r.Context()
	cfg := cfg(ctx)
	storage := storage(ctx)
	w.Header().Set("Server", cfg.name)

	vars := mux.Vars(r)
	alias := vars["alias"]
	u, err := storage.GetLocalUser(alias)
	if err != nil {
		// TODO: return Reject?
		return err
	}

	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		log.Printf("Can't dump: %v", err)
	} else {
		log.Printf("Rec'd! %q", dump)
	}

	var m map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&m); err != nil {
		return err
	}

	a := streams.NewAccept()
	p := localUserAsPerson(u, cfg)
	var to *url.URL
	var isFollow, isUnfollow, isAccept bool
	fullActor := &as.Person{}
	var remoteUser *models.User

	res := &streams.Resolver{
		FollowCallback: func(f *streams.Follow) error {
			isFollow = true

			// 1) Use the Follow concrete type here
			// 2) Errors are propagated to res.Deserialize call below
			m["@context"] = []string{as.Namespace}
			b, _ := json.Marshal(m)
			log.Printf("INFO: Follow: %s", b)

			a.AppendObject(f.Raw())
			_, to = f.GetActor(0)
			obj := f.Raw().GetObjectIRI(0)
			a.AppendActor(obj)

			// First get actor information
			if to == nil {
				return fmt.Errorf("No valid `to` string")
			}
			fullActor, remoteUser, err = fetchActor(storage, to.String())
			if err != nil {
				return err
			}
			w.WriteHeader(http.StatusAccepted)
			return json.NewEncoder(w).Encode(nil)
		},
		UndoCallback: func(u *streams.Undo) error {
			isUnfollow = true

			m["@context"] = []string{as.Namespace}
			b, _ := json.Marshal(m)
			log.Printf("INFO: Undo: %s", b)

			a.AppendObject(u.Raw())
			var res streams.Resolution
			res, to = u.GetActor(0)
			// TODO: get actor from object.object, not object
			obj := u.Raw().GetObjectIRI(0)
			a.AppendActor(obj)
			if res != streams.Unresolved {
				// Populate fullActor from DB?
				remoteUser, err = storage.GetActor(to.String())
				if err != nil {
					log.Printf("ERROR: get actor: %s", err)
					return err
				} else {
					fullActor = remoteUser.AsPerson()
				}
			} else {
				log.Printf("ERROR: No to on Undo!")
			}
			w.WriteHeader(http.StatusAccepted)
			return json.NewEncoder(w).Encode(m)
		},
		AcceptCallback: func(f *streams.Accept) error {
			isAccept = true

			b, _ := json.Marshal(m)
			log.Printf("INFO: Accept: %s", b)
			_, actorIRI := f.GetActor(0)
			fullActor, remoteUser, err = fetchActor(storage, actorIRI.String())
			w.WriteHeader(http.StatusAccepted)
			return json.NewEncoder(w).Encode(nil)
		},
	}
	if err := res.Deserialize(m); err != nil {
		// 3) Any errors from #2 can be handled, or the payload is an unknown type.
		log.Printf("ERROR: Unable to resolve Follow: %v", err)
		log.Printf("ERROR: Map: %s", m)
		return err
	}

	if isAccept {
		if err := storage.AddFollow(u.ID, remoteUser.ID); err != nil {
			log.Printf("ERROR: Couldn't add follower in DB on accept: %v\n", err)
			return err
		}
	}

	go func() {
		time.Sleep(2 * time.Second)
		am, err := a.Serialize()
		if err != nil {
			log.Printf("ERROR: Unable to serialize Accept: %v", err)
			return
		}
		am["@context"] = []string{as.Namespace}

		if to == nil {
			log.Printf("ERROR: No to! %v", err)
			return
		}
		err = makeActivityPost(p, fullActor.Inbox, am)
		if err != nil {
			log.Printf("ERROR: Unable to make activity POST: %v", err)
			return
		}

		if isFollow {
			var followerID string
			if remoteUser != nil {
				followerID = remoteUser.ID
			} else {
				followerID, err = storage.AddUser(fullActor)
				if err != nil {
					return
				}
			}

			// Add follow
			err = storage.AddFollow(followerID, u.ID)
			if err != nil {
				log.Printf("ERROR: Couldn't add follower in DB: %v\n", err)
				return
			}
		} else if isUnfollow {
			// Remove follower locally
			//_, err = app.db.Exec("DELETE FROM follows WHERE followee = ? AND follower = (SELECT id FROM users WHERE actor_id = ?)", u.ID, to.String())
			err = storage.RemoveFollow(u.ID, to.String())
			if err != nil {
				log.Printf("ERROR: Couldn't remove follower from DB: %v\n", err)
			}
		}
	}()

	return nil
}

func fetchActor(storage models.Storage, actorIRI string) (*as.Person, *models.User, error) {
	log.Printf("INFO: Fetching actor %s locally", actorIRI)
	actor := &as.Person{}
	remoteUser, err := storage.GetActor(actorIRI)
	if err != nil {
		if errors.Is(err, models.ErrNotFound) {
			// Fetch remote actor
			log.Printf("INFO: Not found; fetching actor %s remotely", actorIRI)
			actorResp, err := resolveIRI(actorIRI)
			if err != nil {
				log.Printf("ERROR: Unable to get actor! %v", err)
				return nil, nil, fmt.Errorf("Couldn't fetch actor.")
			}
			if err := json.Unmarshal(actorResp, &actor); err != nil {
				// FIXME: Hubzilla has an object for the Actor's url: cannot unmarshal object into Go struct field Person.url of type string
				log.Printf("ERROR: Unable to unmarshal actor! %v", err)
				return nil, nil, fmt.Errorf("Couldn't parse actor.")
			}
		} else {
			return nil, nil, err
		}
	} else {
		actor = remoteUser.AsPerson()
	}
	// TODO: don't return all three; just (*User, error)
	return actor, remoteUser, nil
}

func makeActivityPost(p *as.Person, url string, m interface{}) error {
	log.Printf("INFO: POST %s", url)
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}

	r, _ := http.NewRequest("POST", url, bytes.NewBuffer(b))
	r.Header.Add("Content-Type", "application/activity+json")
	r.Header.Set("User-Agent", "TODO")

	// Sign using the 'Signature' header
	privKey, err := decodePrivateKey(p.GetPrivKey())
	if err != nil {
		return err
	}
	signer := httpsig.NewSigner(p.PublicKey.ID, privKey, httpsig.RSASHA256, nil)
	err = signer.SignSigHeader(r)
	if err != nil {
		log.Printf("ERROR: Can't sign: %v", err)
	}

	dump, err := httputil.DumpRequestOut(r, true)
	if err != nil {
		log.Printf("ERROR: Can't dump: %v", err)
	} else {
		log.Printf("INFO: %s", dump)
	}

	resp, err := http.DefaultClient.Do(r)
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Printf("INFO: Status  : %s", resp.Status)
	log.Printf("INFO: Response: %s", body)

	return nil
}

func resolveIRI(url string) ([]byte, error) {
	log.Printf("INFO: GET %s", url)

	r, _ := http.NewRequest("GET", url, nil)
	r.Header.Add("Accept", "application/activity+json")
	r.Header.Set("User-Agent", "TODO")

	dump, err := httputil.DumpRequestOut(r, true)
	if err != nil {
		log.Printf("ERROR: Can't dump: %v", err)
	} else {
		log.Printf("INFO: %s", dump)
	}

	resp, err := http.DefaultClient.Do(r)
	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	log.Printf("INFO: Status  : %s", resp.Status)
	log.Printf("INFO: Response: %s", body)

	return body, nil
}

func decodePrivateKey(k []byte) (crypto.PrivateKey, error) {
	block, _ := pem.Decode(k)
	if block == nil || block.Type != "RSA PRIVATE KEY" {
		return nil, fmt.Errorf("failed to decode PEM block containing private key")
	}

	return parsePrivateKey(block.Bytes)
}

func parsePrivateKey(der []byte) (crypto.PrivateKey, error) {
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		return key, nil
	}
	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey:
			return key, nil
		default:
			return nil, fmt.Errorf("found unknown private key type in PKCS#8 wrapping")
		}
	}
	if key, err := x509.ParseECPrivateKey(der); err == nil {
		return key, nil
	}

	return nil, fmt.Errorf("failed to parse private key")
}
